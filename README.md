# BibSonomy/PUMA CSL – Publications & Tag Cloud Widget

[BibSonomy/PUMA CSL – Publications & Tag Cloud Widget](https://wordpress.org/plugins/bibsonomy-csl/) is a WordPress extension to include publication lists from the social bookmarking and publication sharing systems [BibSonomy](https://www.bibsonomy.org), (academic) [PUMA](https://www.academic-puma.de) or from other sources.


## SVN Repository

[Official WordPress repository](https://plugins.trac.wordpress.org/browser/bibsonomy-csl/) for the plugin release.

## Development

Check out the wiki for more information on developing and deploying this plugin [here](https://bitbucket.org/bibsonomy/bibsonomy-wordpress/wiki).