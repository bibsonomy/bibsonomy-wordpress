# TODO IN WORKS!!

# Confirm latest version has been tagged:
read -p "Did you tag the latest version of the plugin? (y/n) " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

PLUGIN_KEY="bibsonomy-csl"
# TODO check if tag was found
git fetch --tags
TAG=$(git describe --tags `git rev-list --tags --max-count=1`)
BUILD_DIR=".build"

# Create build dir
mkdir -p "$BUILD_DIR"

cd "$BUILD_DIR"
DIR=$(pwd)
echo "Using build dir $DIR"

echo "Checking out SVN shallowly to $DIR"
svn -q checkout "https://plugins.svn.wordpress.org/$PLUGIN_KEY/" --depth=empty "$DIR"
echo "Done!"

echo "Checking out SVN trunk to $DIR/trunk"
svn -q up trunk
echo "Done!"

echo "Checking out SVN tags shallowly to $DIR/tags"
svn -q up tags --depth=empty
echo "Done!"

echo "Deleting everything in trunk "
rm -r trunk/*
echo "Done!"

echo "Checking out $TAG into trunk"
cp -r ../.git trunk/
cd trunk
git checkout $tag -b release
echo "Done!"

echo "Install composer requirements"
composer install --ignore-platform-reqs --no-dev
echo "Done!"

echo "Move src folder content into trunk"
mv src/* .
rm -r src composer.json composer.lock README.md
echo "Done!"

echo "Remove extra CSL stylesheets"
rm -r vendor/citation-style-language/styles/dependent
echo "Done!"

cd ..

echo "Add new version tag $TAG"
cp -r trunk ./$TAG
mv $TAG tags

#echo "Enter your repository username: "
#read username

echo "Add all files for revision"
# svn add --force trunk/
# svn add --force tags/$TAG
echo "Done!"

echo "Commit to repository"
# svn commit -m "Release $TAG" --username $username
echo "Done!"

echo "You can now delete $BUILD_DIR"